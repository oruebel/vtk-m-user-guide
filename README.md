# VTK-m User's Guide
This repository contains a LaTeX document that is the VTK-m User's Guide user’s 
guide. 

The user's guide contains multiple VTK-m code examples. These examples can be 
compiled and run for learning purposes and to ensure that they are correct. 

The VTK-m User Guide provides everything needed to get up and running with 
VTK-m as well as information on advanced Development and the inner workings
of VTK-m.

## Building The Latex Document

### Required software
In order to build the Latex document you will need to have the following 
installed:

1. [CMake](https://cmake.org/)
2. [LaTeX](https://www.latex-project.org/)
3. [ImageMagick](https://www.imagemagick.org/)

If using the texlive distribution of LaTeX you will need to install 
the following packages.
```
Older Distributions: 

texlive-basic
texlive-bin-extra
texlive-fonts-recommended
texlive-latex-extra
texlive-math-science
texlive-pstricks
---
2019-02-25 Named Distributions

texlive-latex-base
texlive-latex-extra
texlive-extra-utils
texlive-science
texlive-pstricks
texlive-fonts-recommended
```

### Building  the latex
Cmake has been set up to build out the latex document.

1. If you have not done so yet, clone the git repository to your local machine.
You will need [gitlfs](https://git-lfs.github.com/) installed to pull certain large files.
```bash
    git lfs install
    git clone https://gitlab.kitware.com/vtk/vtk-m-user-guide.git
```

2. Create a new directory and then run `cmake` to perform an "out of source
build."
```bash
    $ mkdir vtk-m-user-guide-out
    $ cd vtk-m-user-guide-out
    $ cmake -G "Unix Makefiles" ../vtk-m-user-guide
```
3. Once the CMake generator completes, you can compile in the configured 
build directory (with make or whatever build system selected in the
previous step).
```bash
    $ make
```

## Building the C++ Examples

The VTK-m User's Guide contains many C++ examples. To ensure that all the
examples are correct, it is also possible to build and run them. This step is
not necessary to build the pdf file (as described in the section above), but
it is recommended for anyone providing or changing examples.

### Required Software

In order to successfully build the C++ examples you will need to have the
following additional software packages installed. 

1. [TBB](https://www.threadingbuildingblocks.org/) 

### Configuring the build to compile examples

By default, the repository will be configured without compiling the C++ 
examples. To turn on compiling the C++ examples, you will have to change some
CMake variables.The easiest way to do this is by using the `ccmake` command 
or the CMake GUI.

1. Launch the `ccmake` command in the build directory (`vtk-m-user-guide-out` 
in the previous example) or run the CMake GUI and point it to that build
directory.
2. Change the `BUILD_EXAMPLES` variable to `ON`
3. Run configure (press `c` if running `ccmake` or hit the configure button
in the GUI).
4. The configure pass will probably return with an error about not knowing
where VTK-m is. Set the `VTKm_DIR` to the build directory where the 
`VTKmConfig.cmake` file is located (usually something like 
`vtk-m/build/lib/cmake/vtkm-1.2`).
5. Configure again and then generate.
6. After this configuration is complete, you can compile by running make 
(or whatever build system configured with CMake). The C++ examples will be
built as part of the default compile, but you can compile just them 
with the `ExampleTests` target.
```bash
    $ make ExampleTests
```    

7. After compilation you can use `ctest` to run the examples.

## Hints for environment setup

Building this document requires LaTeX and several other build tools. 
There is no prescriptive way to do this, but here are some system setups
that have been shown to work for previous users.

### Macports on Mac OS

We have had good luck using the macports package manager when operating
on Mac OS. Other package managers exist (such as homebrew) and likely 
any one should work, but here is a primer on getting set up with macports.

1. Install Xcode from the app store.
2. Install command line tools from terminal.
	```bash
	xcode-select --install
	```

3. Install [Macports](https://www.macports.org/install.php)

4. Ports to install for building the user's guide.
```
cmake
ImageMagick
texlive-basic
texlive-bin-extra
texlive-fonts-recommended
texlive-latex-extra
texlive-math-science
texlive-pstricks
```

5. follow Building the C++ Examples and latex described above
